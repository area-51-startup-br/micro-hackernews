drop table comentarios cascade;
drop table votos cascade;
drop table topicos cascade;
drop table usuarios cascade;

create table usuarios (
    id serial not null primary key,
    apelido varchar(20) not null,
    email varchar(100) not null,
    senha varchar(255) not null,
    ativo boolean not null,
    auth_token text,
    criacao timestamp not null default (now() at time zone 'utc')
);

create table topicos (
    id serial not null primary key,
    usuario_id int not null references usuarios (id),
    titulo varchar(64) not null,
    descricao varchar(1024) not null,
    criacao timestamp not null default (now() at time zone 'utc')
);

create table votos (
    topico_id int not null references topicos (id),
    usuario_id int not null references usuarios (id),
    criacao timestamp not null default (now() at time zone 'utc')
);

create table comentarios (
    id serial not null primary key,
    usuario_id int not null references usuarios (id),
    topico_id int not null references topicos (id),
    comentario_pai_id int references comentarios (id),
    texto text not null,
    criacao timestamp not null default (now() at time zone 'utc')
);
