async function postar (url, form = null) {
    const data = form !== null ? new URLSearchParams(new FormData(form)) : null

    const res = await fetch(url, { method: 'post', body: data })

    if (res.status !== 200) {
        return [ res.statusText ]
    }

    return await res.json()
}

function informarErros (div, erros) {
    for (const erro of erros) {
        const span = document.createElement('span')
        span.classList.add('erro')
        span.textContent = erro
        div.appendChild(span)
    }
}

function get (id) {
    return document.getElementById(id)
}

function removerFilhos (elemento) {
    while (elemento.firstChild) {
        elemento.removeChild(elemento.firstChild)
    }
}
