async function criarComentario (e, topicoId) {
    e.preventDefault()

    removerFilhos(get('erros-comentar'))

    const res = await postar('/topico/comentar/' + topicoId, get('form-criar-comentario'))

    if (res.erros.length === 0) {
        document.getElementById('texto-comentario').value = ''
        location.reload()
    } else {
        informarErros(get('erros-comentar'), res.erros)
    }
}

async function votar (topicoId) {
    const res = await postar('/topico/votar/' + topicoId)

    if (res.erros.length === 0) {
        location.reload()
    } else {
        informarErros(get('erros-votar'), res.erros)
    }
}
