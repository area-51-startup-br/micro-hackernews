<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// HomeController
$router->get('/', 'HomeController@get_index');

// UsuarioController
$router->get('/usuario/criar', 'UsuarioController@get_criar');
$router->post('/usuario/criar', 'UsuarioController@post_criar');

$router->get('/usuario/entrar', 'UsuarioController@get_entrar');
$router->post('/usuario/entrar', 'UsuarioController@post_entrar');

$router->get('/usuario/sair', 'UsuarioController@get_sair');

$router->get('/usuario/visualizar/{apelido}', 'UsuarioController@get_visualizar');

//TopicoController
$router->get('/topico/criar', 'TopicoController@get_criar');
$router->post('/topico/criar', 'TopicoController@post_criar');

$router->get('/topico/visualizar/{id}', 'TopicoController@get_visualizar');

$router->post('/topico/comentar/{id}', 'TopicoController@post_comentar');

$router->post('/topico/votar/{id}', 'TopicoController@post_votar');
