# Micro HackerNews

Um micro fórum!

## Requisitos

* PHP 7.2+
* composer 1.6.3+
* ext-mbstring
* ext-dom
* postgresql
* php-pgsql

## Ao clonar o repositório

* Rodar `psql --username=postgres --command="create database micro_hackernews"` para criar o banco
* Copiar o arquivo `.env.example` para `.env` e ajustar os parâmetros de conexão com banco
* Abrir o arquivo `php.ini` presente na pasta do PHP e descomentar a extensão `pgsql`

## Ao atualizar o repositório

* Rodar `composer install` para instalar as dependências

## Rotina de desenvolvimento

Iniciar a aplicação com:

    php -S localhost:8000 -t public

Aplicar migration:

    psql --username=<user> --dbname=micro_hackernews --file=database/migrations/<arquivo>.sql
