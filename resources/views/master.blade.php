<!DOCTYPE html>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="/css/bulma.css">
<link rel="stylesheet" href="/css/master.css">
@yield('estilos')

<title>Hacker News</title>

<nav class="navbar is-transparent">
    <div class="navbar-menu centralizar-texto">
        <a class="navbar-item" href="/">Home</a>
        @if ($esta_logado)
            <a class="navbar-item" href="/topico/criar">Criar tópico</a>
            <a class="navbar-item" href="/usuario/sair">Sair</a>
        @else
            <a class="navbar-item" href="/usuario/entrar">Entrar</a>
            <a class="navbar-item" href="/usuario/criar">Criar conta</a>
        @endif
    </div>
</nav>

<div class="content">
    @yield('conteudo')
</div>

<script src="/js/master.js"></script>
@yield('scripts')

{{-- <footer class="footer">
    <div>Icons made by <a href="https://www.flaticon.com/authors/good-ware" title="Good Ware">Good Ware</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
</footer> --}}
