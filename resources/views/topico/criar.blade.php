@extends('master')

@section('estilos')
    <link rel="stylesheet" href="/css/topico/criar.css">
@endsection

@section('conteudo')
    <div class="centralizar-texto">
        <div class="title">Criar tópico</div>
        <div class="subtitle">Compartilhe algo relevante para as pessoas</div>
    </div>

    <form action="/topico/criar" method="post" class="centralizar">
        <div class="field">
            <label class="label">Título</label>
            <div class="control">
                <input class="input" type="text" placeholder="Título" name="titulo" value="{{$modelo->titulo}}">
            </div>
        </div>

        <div class="field">
            <label class="label">Descrição</label>
            <div class="control">
                <textarea class="textarea" name="descricao" placeholder="Descrição">{{$modelo->descricao}}</textarea>
            </div>
        </div>

        @if ($erros)
            @component('componentes/lista-erros', ['erros' => $erros])
            @endcomponent
        @endif

        <div class="centralizar-texto">
            <input class="button is-primary" type="submit" value="Criar">
        </div>
    </form>
@endsection
