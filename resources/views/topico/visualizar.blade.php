@extends('master')

@section('estilos')
    <link rel="stylesheet" href="/css/topico/visualizar.css">
@endsection

@section('conteudo')
    <div id="erros-votar"></div>

    <div class="topico card centralizar">
        <div class="card-content">
            <div class="media-content centralizar-texto">
                @if ($modelo->pode_votar)
                    <button class="button" onclick="votar({{$modelo->id}})">Votar</button>
                @else
                    <button class="button" disabled>Votar</button>
                @endif

                <div class="linha-votos">
                    @if ($modelo->votos === 0)
                        Nenhum voto até o momento
                    @else
                        <span class="tag">{{$modelo->votos}}</span>
                        @if ($modelo->votos === 1)
                            voto
                        @else
                            votos
                        @endif
                    @endif
                </div>
                <p class="title is-4">
                    {{$modelo->titulo}}
                </p>
                <p class="subtitle is-6">
                    Criado por <a href="/usuario/visualizar/{{$modelo->autor}}">{{$modelo->autor}}</a> em {{$modelo->criacao}}
                </p>
            </div>

            <hr/>

            <div class="descricao content">{{$modelo->descricao}}</div>
        </div>
    </div>

    <hr/>

    <form id="form-criar-comentario" class="centralizar">
        @if ($esta_logado)
            <div class="field">
                <div class="control">
                    <textarea class="textarea" name="texto" id="texto-comentario" placeholder="Comentário"></textarea>
                </div>
            </div>

            <div class="centralizar-texto">
                <input class="button is-primary" type="submit" value="Comentar" onclick="criarComentario(event, {{$modelo->id}})">
            </div>

            <div id="erros-comentar"></div>
        @else
            <div class="field">
                <div class="control">
                    <textarea disabled class="textarea" placeholder="Comentário"></textarea>
                </div>
            </div>

            <div class="centralizar-texto">
                <input disabled class="button is-primary" type="submit" value="Comentar">
            </div>
        @endif
    </form>

    <hr/>

    <div class="box centralizar" id="listagem-comentarios">
        @forelse ($modelo->comentarios as $comentario)
            <div class="comentario">
                <div>Comentado por <a href="/usuario/visualizar/{{$comentario->autor}}">{{$comentario->autor}}</a> em {{$comentario->criacao}}</div>
                <div>{{$comentario->texto}}</div>
            </div>
        @empty
            <article class="message is-warning">
                <div class="message-body">
                    Nenhum comentário por enquanto
                </div>
            </article>
        @endforelse
    </div>
@endsection

@section('scripts')
    <script src="/js/topico/visualizar.js"></script>
@endsection
