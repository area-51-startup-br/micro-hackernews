@extends('master')

@section('conteudo')
    <div class="centralizar-texto">
        <div class="title">{{$modelo->apelido}}</div>
        <div class="subtitle">Cadastrado em {{$modelo->criacao}}</div>
    </div>

    <nav class="breadcrumb is-centered" aria-label="breadcrumbs">
        <ul id="abas">
            <li id="li-topicos" class="is-active">
                <a onclick="clicouAba(event)">Tópicos</a>
            </li>
            <li id="li-comentarios">
                <a onclick="clicouAba(event)">Comentários</a>
            </li>
            <li id="li-votos">
                <a onclick="clicouAba(event)">Votos</a>
            </li>
        </ul>
    </nav>
@endsection

@section('scripts')
    <script src="/js/usuario/visualizar.js"></script>
@endsection
