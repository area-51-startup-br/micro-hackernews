@extends('master')

@section('estilos')
    <link rel="stylesheet" href="/css/usuario/criar.css">
@endsection

@section('conteudo')
    <div class="centralizar-texto">
        <div class="title">Acessar conta</div>
        <div class="subtitle">Crie tópicos, comente e vote</div>
    </div>

    <form action="/usuario/entrar" method="post" class="centralizar">
        <div class="field">
            <label class="label">Apelido</label>
            <div class="control">
                <input class="input" type="text" placeholder="Apelido" name="apelido" value="{{$modelo->apelido}}">
            </div>
        </div>

        <div class="field">
            <label class="label">Senha</label>
            <div class="control">
                <input class="input" type="password" placeholder="Senha" name="senha" value="{{$modelo->senha}}">
            </div>
        </div>

        @if ($erros)
            @component('componentes/lista-erros', ['erros' => $erros])
            @endcomponent
        @endif

        <div class="centralizar-texto">
            <input class="button is-primary" type="submit" value="Entrar">
        </div>
    </form>
@endsection
