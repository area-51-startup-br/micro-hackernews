@extends('master')

@section('estilos')
    <link rel="stylesheet" href="/css/home/index.css">
@endsection

@section('conteudo')
    @forelse ($topicos as $topico)
        @component('componentes/cartao-topico', [ 'topico' => $topico ])
        @endcomponent
    @empty
        <article class="centralizar aviso-sem-topicos message is-warning">
            <div class="message-body">
                Nenhum tópico por enquanto
            </div>
        </article>
    @endforelse
@endsection
