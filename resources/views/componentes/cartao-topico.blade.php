<div class="box cartao-topico centralizar">
    <div>
        <span class="tag">{{$topico->votos}}</span>
        <a href="/topico/visualizar/{{$topico->id}}">{{$topico->titulo}}</a>
    </div>
    <div>
        Criado por <a href="/usuario/visualizar/{{$topico->autor}}">{{$topico->autor}}</a> em {{$topico->criacao}}
    </div>
</div>
