<article class="message is-danger">
    <div class="message-header">
        @if (count($erros) === 1)
            Erro
        @else
            Erros
        @endif
    </div>
    <div class="message-body">
        @if (count($erros) === 1)
            {{$erros[0]}}
        @else
            <ul>
                @foreach ($erros as $erro)
                    <li>{{$erro}}</li>
                @endforeach
            </ul>
        @endif
    </div>
</article>
