<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TopicoController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_criar()
    {
        if (Auth::user() === null) {
            abort(403, 'Você deve estar logado para criar um tópico');
        }

        $modelo = (object) [
            'titulo' => '',
            'descricao' => ''
        ];

        return view('topico.criar', [
            'erros' => null,
            'modelo' => $modelo
        ]);
    }

    public function post_criar(Request $request)
    {
        if (Auth::user() === null) {
            abort(403, 'Você deve estar logado para criar um tópico');
        }

        $modelo = (object) $request->only('titulo', 'descricao');
        $erros = [];

        $modelo->titulo = trim($modelo->titulo);
        $modelo->descricao = trim($modelo->descricao);

        if (strlen($modelo->titulo) < 3 || strlen($modelo->titulo) > 64) {
            $erros[] = 'Título inválido';
        }

        if (strlen($modelo->descricao) < 3 || strlen($modelo->descricao) > 1024) {
            $erros[] = 'Descrição inválida';
        }

        if ($erros) {
            return view('topico.criar', [
                'erros' => $erros,
                'modelo' => $modelo
            ]);
        }

        DB::insert('insert into topicos (usuario_id, titulo, descricao) values (?, ?, ?)', [
            Auth::user()->id,
            $modelo->titulo,
            $modelo->descricao
        ]);

        return redirect('/');
    }

    public function get_visualizar($id)
    {
        $resultados = DB::select("
select
    t.id,
    t.titulo,
    to_char(t.criacao, 'DD/MM/YYYY HH24:MI') criacao,
    u.apelido autor,
    coalesce(v.contagem, 0) votos,
    t.descricao
from
    topicos t
    join usuarios u on t.usuario_id = u.id
    left join (
        select v.topico_id, count(*) contagem
        from votos v
        group by topico_id
    ) v on v.topico_id = t.id
where
    t.id = ?", [ $id ]);

        if (count($resultados) === 0) {
            abort(404, 'Tópico não encontrado');
        }

        $modelo = $resultados[0];
        $modelo->votos = (int)$modelo->votos;

        $comentarios = DB::select("
select
    u.apelido autor,
    to_char(c.criacao, 'DD/MM/YYYY HH24:MI') criacao,
    c.texto
from
    comentarios c
    join usuarios u on c.usuario_id = u.id
where
    c.topico_id = ?
order by
    c.criacao desc", [ $id ]);

        $modelo->comentarios = $comentarios;

        $usuario = Auth::user();

        if ($usuario !== null) {
            $resultados = DB::select("
            select coalesce(count(*), 0) v
            from votos
            where topico_id = ? and usuario_id = ?", [ $id, $usuario->id ]);

            $modelo->pode_votar = ((int)($resultados[0]->v)) === 0;
        } else {
            $modelo->pode_votar = false;
        }

        return view('topico.visualizar', [ 'modelo' => $modelo ]);
    }

    public function post_comentar(Request $request, $id)
    {
        $usuario = Auth::user();

        if ($usuario === null) {
            abort(403, 'Você deve estar logado para comentar');
        }

        $texto = trim($request->only('texto')['texto']);
        $erros = [];

        if (strlen($texto) < 10) {
            $erros[] = 'Texto muito curto';
        }

        if (preg_match('/^\d+$/', $id) !== 1) {
            $erros[] = 'Tópico inválido';
        }

        if ($erros) {
            return response()->json(['erros' => $erros]);
        }

        DB::insert('insert into comentarios (usuario_id, topico_id, texto) values (?, ?, ?)', [
            $usuario->id,
            $id,
            $texto
        ]);

        return response()->json(['erros' => []]);
    }

    public function post_votar($id)
    {
        $usuario = Auth::user();

        if ($usuario === null) {
            abort(403, 'Você deve estar logado para comentar');
        }

        if (preg_match('/^\d+$/', $id) !== 1) {
            return response()->json(['erros' => ['Tópico inválido']]);
        }

        DB::insert('insert into votos (usuario_id, topico_id) values (?, ?)', [
            $usuario->id,
            $id
        ]);

        return response()->json(['erros' => []]);
    }
}
