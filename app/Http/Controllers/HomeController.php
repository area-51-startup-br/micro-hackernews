<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_index(Request $request)
    {
        $topicos = DB::select("
select
    t.id,
    t.titulo,
    to_char(t.criacao, 'DD/MM/YYYY HH24:MI') criacao,
    u.apelido autor,
    coalesce(v.contagem, 0) votos
from
    topicos t
    join usuarios u on t.usuario_id = u.id
    left join (
        select v.topico_id, count(*) contagem
        from votos v
        group by topico_id
    ) v on v.topico_id = t.id
order by
    t.criacao desc"
);

        return view('home.index', [
            'topicos' => $topicos
        ]);
    }
}
