<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Ramsey\Uuid\Uuid;

class UsuarioController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_sair(Request $request)
    {
        $usuario = $request->user();

        if ($usuario === null) {
            return redirect('/');
        }

        DB::update('update usuarios set auth_token = null where id = ?', [ $usuario->id ]);

        return redirect('/'); //->withCookie(new Cookie('auth', '', 1));
    }

    public function get_entrar()
    {
        $modelo = (object) [
            'apelido' => '',
            'senha' => ''
        ];

        return view('usuario.entrar', [
            'erros' => null,
            'modelo' => $modelo
        ]);
    }

    public function post_entrar(Request $request)
    {
        $modelo = (object) $request->only('apelido', 'senha');

        $resultado = DB::select('select ativo, senha from usuarios where apelido = ? and ativo', [$modelo->apelido]);

        if (empty($resultado) || !password_verify($modelo->senha, $resultado[0]->senha)) {
            return view('usuario.entrar', [
                'erros' => [ 'Usuário não encontrado.' ],
                'modelo' => $modelo
            ]);
        }

        $token = Uuid::uuid4()->toString();

        DB::update('update usuarios set auth_token = ? where apelido = ?', [ $token, $modelo->apelido ]);

        return redirect('/')->withCookie(new Cookie('auth', $token));
    }

    public function get_criar()
    {
        $modelo = (object) [
            'apelido' => '',
            'email' => '',
            'senha' => ''
        ];

        return view('usuario.criar', [
            'erros' => null,
            'modelo' => $modelo
        ]);
    }

    public function post_criar(Request $request)
    {
        $modelo = (object) $request->only('apelido', 'email', 'senha');
        $erros = [];

        if (preg_match('/^\w{5,20}$/', $modelo->apelido) !== 1) {
            $erros[] = 'Apelido inválido';
        } else if (!$this->verificar_disponivel('apelido', $modelo->apelido)) {
            $erros[] = 'Apelido já utilizado';
        }

        if (!filter_var($modelo->email, FILTER_VALIDATE_EMAIL)) {
            $erros[] = 'E-mail inválido';
        } else if (!$this->verificar_disponivel('email', $modelo->email)) {
            $erros[] = 'E-mail já utilizado';
        }

        if (preg_match('/^\w{5,20}$/', $modelo->senha) !== 1) {
            $erros[] = 'Senha inválida';
        }

        if ($erros) {
            return view('usuario.criar', [
                'erros' => $erros,
                'modelo' => $modelo
            ]);
        }

        DB::insert('insert into usuarios (apelido, email, senha, ativo) values (?, ?, ?, true)', [
            $modelo->apelido,
            $modelo->email,
            password_hash($modelo->senha, PASSWORD_DEFAULT)
        ]);

        return redirect('/usuario/entrar');
    }

    public function get_visualizar($apelido)
    {
        $resultado = DB::select("
            select to_char(criacao, 'DD/MM/YYYY HH24:MI') criacao
            from usuarios
            where apelido = ?", [ $apelido ]);

        if (count($resultado) === 0) {
            abort(404, 'Usuário não encontrado');
        }

        $modelo = $resultado[0];
        $modelo->apelido = $apelido;

        return view('usuario.visualizar', [ 'modelo' => $modelo ]);
    }

    private function verificar_disponivel($campo, $valor)
    {
        $query = "select count(*) c from usuarios where {$campo} = ?";
        $resultado = DB::select($query, [ $valor ]);

        return ((int)($resultado[0]->c)) === 0;
    }
}
